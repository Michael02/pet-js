# PET-JS

### Project Environment Tool (in Node.JS)

Heavily inspired by [github@limebrails/pet](https://github.com/limebrains/pet), tool to manage and start projects or
change between them.

## Usage

```shell
| => pet -h
pet <project>

go to project and prepare environment

Commands:
  pet init              initiate new PET project in current directory
  pet <project>         go to project and prepare environment          [default]
  pet list              list all projects
  pet edit [project]    edit project script
  pet remove <project>  remove project from PET.
  pet nuke              remove whole PET from your system

Positionals:
  project  specify which project to start                               [string]

Options:
  -d, --debug    Show stack on error                                   [boolean]
  -h, --help     Show help                                             [boolean]
  -v, --version  Show version number                                   [boolean]
```

`pet init` initiate new PET project with the default name of current directory, you can pass a name argument with your
own name for the project. It creates a file and starts up `nano` where you can add commands to prepare environment.

```shell
pet init --name [your name for project]

# or

pet init -n [your name for project]
```

## Installation

you can install pet with
```shell
npm -g i ssh://git@gitlab.com:Michael02/pet-js.git
```
after which, you can remove package, the pet command will still work, 
<br/>to uninstall command run `pet nuke`

or, clone the repo, and run bin/install

```shell
git clone git@gitlab.com:Michael02/pet-js.git
cd pet-js
node bin/install
```
