import yargs from "yargs";
import { hideBin } from "yargs/helpers";
import * as path from "path";
import * as fs from "fs-extra";
import {
  checkName,
  checkRoot,
  getAllProjects,
  getProjectScriptPath,
  getShellFilePath,
  readFile,
  removePetFromShell,
  returnCommand,
  writeFile,
} from "./utils";
import { baseProjectScript, petRoot } from "./constants";

import pJSON from "../package.json";

require("dotenv").config();

const PET_ACTIVE = "PET_ACTIVE";

export function cli({ env, argv, stdout }: NodeJS.Process) {
  try {
    checkRoot();
    yargs(hideBin(argv))
      .command(
        "init",
        "initiate new PET project in current directory",
        (args) =>
          args.option("name", {
            alias: "n",
            type: "string",
            describe: "name of project",
            default: path.basename(env.PWD),
          }),
        (args) => {
          checkName(args.name);
          const scriptPath = getProjectScriptPath(args.name, true);
          if (fs.pathExistsSync(scriptPath)) {
            throw new Error(`Project named "${args.name}" already exists.`);
          }
          writeFile(scriptPath, baseProjectScript(env.PWD), { mode: 0o644 });
          returnCommand(`nano ${scriptPath}`);
        },
      )

      .command(
        "$0 <project>",
        "go to project and prepare environment",
        (args) =>
          args.positional("project", {
            type: "string",
            describe: "specify which project to start",
          }),
        (args) => {
          const scriptPath = getProjectScriptPath(args.project);
          let data = readFile(scriptPath);
          data = data.replace(/^#.*$|^\s*$/gm, "").trim();
          data += `\nexport ${PET_ACTIVE}="${args.project}"`;
          returnCommand(data);
        },
      )

      .command("list", "list all projects", {}, () => {
        const list = getAllProjects();
        if (!list.length) {
          stdout.write("You have no projects.\n");
        } else {
          stdout.write(`Your PET projects: \n${list.map((p) => `${p}`).join("\n")}\n`);
        }
      })

      .command(
        "edit [project]",
        "edit project script",
        (args) =>
          args.positional("project", {
            type: "string",
            describe: "specify which project to edit",
            demandOption: false,
          }),
        (args) => {
          args.project = args.project || env[PET_ACTIVE];
          if (!args.project) {
            throw new Error("You are not in active PET project, please provide project name.");
          }
          const scriptPath = getProjectScriptPath(args.project);
          returnCommand(`nano ${scriptPath}`);
        },
      )

      .command(
        "remove <project>",
        "remove project from PET.",
        (args) =>
          args.positional("project", {
            type: "string",
            describe: "specify which project to remove",
          }),
        (args) => {
          const scriptPath = getProjectScriptPath(args.project);
          fs.removeSync(scriptPath);
        },
      )

      .command("nuke", "remove whole PET from your system", {}, () => {
        const shFile = path.basename(getShellFilePath());
        removePetFromShell();
        fs.removeSync(petRoot);
        stdout.write(
          `\n\n#########################\n\nRemoved Pet from your home folder and from ${shFile}.\n\n#########################\n`,
        );
      })

      .option("debug", {
        alias: "d",
        describe: "Show stack on error",
        type: "boolean",
        global: true,
      })

      .completion("completion", false, (current, argv1) => {
        const args = argv1["_"];
        const argsLen = args.length;
        const isCommandWithProject = ["edit", "remove"].includes(args?.[1]);
        const projects = getAllProjects();
        if (argsLen < 3) {
          return [...projects, "init", "edit", "list", "remove"];
        }
        if (argsLen === 3 && isCommandWithProject) {
          return projects;
        }
        return [];
      })

      .version(pJSON.version)
      .demandCommand(1)
      .alias("h", "help")
      .alias("v", "version")
      .parse();
  } catch (err) {
    const isDev = argv.includes("-d") || argv.includes("--debug");
    stdout.write(`${err.message || err}\n${isDev ? err?.stack : ""}`);
    process.exitCode = 1;
  }
}
