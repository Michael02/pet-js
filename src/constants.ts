import * as path from "path";

export const petRoot = process.env.PET_ROOT || path.join(process.env.HOME, ".petjs");

export const petBash = (binPetPath: string) => `
###-begin-pet-###
pet() {
    res=$(node ${binPetPath} $@)
    hasCmd=\${res:0:4}
    if [[ $hasCmd == 'cmd:' ]] ; then
        eval \${res:4}
    else
        echo $res
    fi
}
###-end-pet-###\n
`;

export const baseProjectScript = (path: string) => `
# edit this file to more quickly start up project

clear
cd ${path}
# ls -CGFh

# put your bash command below
`;
