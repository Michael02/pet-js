import { petBash, petRoot } from "./constants";
import * as path from "path";
import {
  copyDir,
  makeDirs,
  writeFile,
  readFile,
  removePetFromShell,
  getShellFilePath,
} from "./utils";
import { execSync } from "child_process";

require("dotenv").config();

export const install = async ({ env, stdout }: NodeJS.Process) => {
  try {
    const rootBinPath = path.join(petRoot, "bin");
    makeDirs(rootBinPath);
    const rootLibPath = path.join(petRoot, "lib");
    const rootProjectsPath = path.join(petRoot, "projects");
    makeDirs(rootProjectsPath);

    const scriptsPath = path.join(env.PWD, "dist");

    const rootPetPath = path.join(rootBinPath, "pet");
    writeFile(rootPetPath, "#!/usr/bin/env node\n\nrequire('../lib/cli').cli(process);", {
      mode: 0o755,
    });

    copyDir(scriptsPath, rootLibPath);
    stdout.write(`Copied files to ${petRoot}\n`);

    const shellFilePath = getShellFilePath();
    const shellFile = path.basename(shellFilePath);
    removePetFromShell();
    let rcFileData = readFile(shellFilePath);
    rcFileData += petBash(rootPetPath);
    writeFile(shellFilePath, rcFileData, { mode: 0o644 });
    execSync(`${rootPetPath} completion >> ${shellFilePath}`);
    stdout.write(`\nAdded bash script to ${shellFile}\n`);
    stdout.write(
      "\n#########################\n\nInstallation Complete\n\n#########################\n",
    );
  } catch (err) {
    stdout.write(err.message || err);
    process.exitCode = 1;
  }
};
