import * as fs from "fs-extra";
import * as path from "path";
import { petRoot } from "./constants";

export const checkName = (name: string, error?: string) => {
  if (!/^[a-zA-Z]\w{2,}$/g.test(name)) {
    throw new Error(
      error ||
        `The name "${name}" is not allowed. \nName can only consist of alpha-numeric characters and underscore (_), cannot start with number and must be at least 3 characters long.`,
    );
  }
};

export const checkPathExists = (path: string, error?: string) => {
  if (!fs.pathExistsSync(path)) {
    throw new Error(error || `Path "${path}" does not exists.`);
  }
};

export const getProjectScriptPath = (project: string, skipCheck?: boolean) => {
  const projectPath = path.join(petRoot, "projects", project);
  if (!skipCheck) {
    checkPathExists(
      projectPath,
      `Project "${project}" does not exists. \nYou can run "pet init" in your project to initialize it.`,
    );
  }
  return projectPath;
};

export const checkRoot = () => {
  checkPathExists(
    petRoot,
    'Pet Root directory does not exists in your HOME folder. Try reinstalling with "yarn installApp".',
  );
};

export const makeDirs = (path: string) => {
  fs.mkdirSync(path, { recursive: true });
};

export const copyDir = (src: string, dest: string) => {
  checkPathExists(src);
  if (!fs.existsSync(dest)) {
    makeDirs(dest);
  }
  fs.copySync(src, dest);
};

export const writeFile = (path: string, data: string, options?: fs.WriteFileOptions) => {
  fs.outputFileSync(path, data, options);
};

export const readFile = (path: string, options?: { encoding: BufferEncoding; flag?: string }) => {
  checkPathExists(path);
  if (!options) {
    options = { encoding: "utf8" };
  }
  return fs.readFileSync(path, options);
};

export const returnCommand = (cmd: string) => process.stdout.write(`cmd:${cmd}`);

export const getAllProjects = () => {
  const projectsPath = getProjectScriptPath("");
  return fs.readdirSync(projectsPath);
};

export const getShellFilePath = () => {
  const shellFile = process.env.SHELL === "/bin/zsh" ? ".zshrc" : ".bashrc";
  return path.join(process.env.HOME, shellFile);
};

export const removePetFromShell = () => {
  const shellFilePath = getShellFilePath();
  let shellFileData = readFile(shellFilePath);
  shellFileData = shellFileData.replace(/\n*###-begin-pet-###[\w\W]*###-end-pet-###\n*/gm, "\n");
  shellFileData = shellFileData.replace(
    /\n*###-begin-pet-completions-###[\w\W]*###-end-pet-completions-###\n*/gm,
    "\n",
  );
  shellFileData = shellFileData.replace(/\n*#compdef pet\n*/gm, "\n");
  writeFile(shellFilePath, shellFileData, { mode: 0o644 });
};
